package de.schneidertim.nmea_navdata_grabber;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

@RequiresApi(api = Build.VERSION_CODES.M)
public class MainActivity extends AppCompatActivity {
    static final boolean DEBUG = BuildConfig.DEBUG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissions(new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION
        }, 1337);
        joinMultiCastGroup();
    }

    public void OnClick(View view){
        if(DEBUG) Log.d("MainActivity","Clicked");
        joinMultiCastGroup();
    }

    void joinMultiCastGroup(){
        TextView txtMultiCastGroup = (TextView) findViewById(R.id.m_text_multicastGroup);
        TextView txtMultiCastPort = (TextView) findViewById(R.id.m_text_multicastPort);

        NmeaGrabberService.joinMultiCastGroup(this,
                txtMultiCastGroup.getText().toString(),
                Integer.valueOf(txtMultiCastPort.getText().toString()));
    }
}