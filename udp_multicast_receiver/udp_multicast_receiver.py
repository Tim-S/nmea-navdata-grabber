#!/usr/bin/env python

import time
import struct
import socket
import sys
import argparse

def main():
    parser = argparse.ArgumentParser(description='Listen to UDP-Multicast-Messages.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--group', default="224.51.105.104",type=str, help='UDP multicast-group')
    parser.add_argument('--port', default="4242",type=int, help='UDP port')
    parser.add_argument('--interface', default="192.168.2.104",type=str, help='Network-Interface used for multicast')
    args = parser.parse_args()
    receiver(args.group, args.port, args.interface)

def receiver(group, port, interface):
    # Create a socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Allow multiple copies of this program on one machine (not strictly needed)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    # Bind it to the port
    s.bind(('', port))

    # Join group
    s.setsockopt(socket.SOL_IP,socket.IP_ADD_MEMBERSHIP,
                socket.inet_aton(group)+socket.inet_aton(interface) )

    # Loop, printing any data we receive
    while True:
        data, sender = s.recvfrom(1500)
        while data[-1:] == '\0': data = data[:-1] # Strip trailing \0's
        print (str(sender) + '  ' + repr(data))


if __name__ == '__main__':
    main()